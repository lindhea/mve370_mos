## Klassiska teorier
### Platon
* Själar tvättade i glömskans flod
    * Man har redan kunskap, men i varierande grad. Under livet återfinner man
      förlorad/dold kunskap.
* Personer fastkedjade i en grotta
    * I okunskap kommer man aldrig få en rättvisande bild av verkligheten,
      endast en projektion av den.
    * Platon säger att resonemanget visar att alla innehar kunskap, den behvöer
      bara upptäckas. Om man släpps fri från grottan så kommer man förstå hur
      världen är funtad...

### Locke
* *Tabula rasa*
    * Vi föds med några få förmågor
    * All kunskap kommer från erfarenhet
    * Jfr. vad som händer om man försöker lära något något som är långt över
      deras kunskapsnivå

Både Platons och Lockes teorier beskriver eleven som passiv. Så är inte
verkligheten.


## Behaviourism
### Klassisk betingning
* Pavolv
* Stimuli --> Respons
    * Negativ stimuli: matte, läsa Shakespear, ...

### Instrumentell betingning
* Lära sig en lösningsmetod (dra i spak, slipp låda)
* Ersätter naturliga stimuli

### B.F. Skinner
* Att alltid få belöning är inte mest effektivt. Om barnen förväntar sig att få
  godis så är det inte lika effektivt som om barnen blir överraskade av att få
  godis.
* Forma beteende med välavvägt utdelande av belöning.


## Problemlösning, insikt och aktivitet
### Den gestaltteoretiska ansatsen
* Undvik omöjliga situationer -- folk ger upp istället för att försöka lära sig
  något.
* Ge istället alla förutsättningar för att lösa problemet: se s. 58, Sultan.
* Det kan vara svårt att greppa meningsfulla helheter. Bryt ned problem i
  beståndsdelar, och ta det steg för steg. (vet inte om teorin faktiskt säger
  så...)

## Kognitiv struktur och läroämnets struktur
### Mentala kartor; konstruktivistisk teori

* Man minns sällan saker på samma sätt som vi lagrar data på papper eller i
  datorer. Ett vanligt sätt som sinnet arbetar på för att förstå och minnas
  saker är att bilda strukturer med referenspunkter, eller som en karta där man
  vet var saker befinner sig i förhållande till vartannat.
* Har man mycket kunskap och erfarenheter blir det lättare att bilda nya länkar
  och referenspunkter för att lära nytt.
* Piaget menar att mentala strukturer byggs genom ett aktiv arbete, och
  framhäver vikten av det aktiva i lärandet (till skillnad från Platon & C.o.).
* Precis som i Git krävs endast ett delta samt en pekare för en ny commit, ifall
  det redan finns en stor commit-historik (sno inte den. Den är min).
* Därför menar Piaget att man måste lära ut från den kunskapsnivå eleven har,
  och intespringa i förväg (då kan Git bli långsamt).

### Kritik kring konstruktivism
* Mentala konstruktioner är inte fysiska konstruktioner, och man ska inte anta
  att alla analogier från den fysiska världen går att överföra till den mentala
  världen.
* Vilken underliggande struktur finns från början?

# Kognitiv struktur enligt Piaget och psykologisk konstruktivism
### John Dewey
* I motsats till Piagets teori(er) så lär vi oss sällan individuellt, utan i ett
  socialt sammanhang. Sådant påverkar.
    * Socialt
    * Gemenskap
    * "Lärarens uppgift är att skapa villkor som stimulerar lärande"

### Vygotskij
* Tycker att elevens utvecklingsstadium inte är så viktigt. Det är mer ett mått
  på vad eleven redan klarar av på egen hand.
* Istället menar Vygotskij att man har en *utvecklingszon*, och för att lära sig
  bra måste man befinna sig i/ta sig till den zonen. En persons potentiella
  utvecklingszon avgör hur mycket mer man kan lära sig ~ish...
* Härma är ett viktigt sätt att lära sig! Och det funkar bättre om man har någon
  att härma...


## Kognitiv struktur

Å ena sidan kan en grupp inte lära sig saker om individerna inte lär sig, men å
andra sidan underlättar grupper mycket för att individerna ska kunna lära sig.

### Mentala kartor och referenspunkter
* Det är ofta bra att ge en översikt först, så att man kan bilda referenser
  innan man faktiskt ska lära sig detaljerna.
* Kan leda fel, ifall man inte fått en rätt bild av kartan från början...

## Kunskapens överförbarhet
### ...

## Den informationstekniska modellen
### Datormodellen
* ???

