\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[swedish]{babel}
\usepackage[iso,swedish]{isodate}
\usepackage{icomma}

%% Usually preferred packages:
\usepackage[hidelinks]{hyperref} % Usage: \url{}
\usepackage[a4paper]{geometry}

\usepackage{natbib}
\bibliographystyle{agsm}
\let\cite\citet

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[R]{\textit{ID: 392}}
\fancyhead[L]{\leftmark}

\title{Inlämning 2\\
    \small{MVE370 -- Matematik och Samhälle}}
%\date{}
\author{ID: 392}

\usepackage[swedish]{todonotes}

\begin{document}

\maketitle

\section{Inledning}
\label{sec:intro}

Att lära sig något är inte alltid lätt. Man kan till och med tänka sig scenarion
där det är oändligt svårt att lära sig en ny färdighet (exempelvis är det
fullkomligt otänkbart att se ett nyfött barn lära sig programmera i
\texttt{C++}\footnote{Möjligen \texttt{Python}...}). Att däremot hitta scenarion
som gör det oändligt lätt att lära sig är i det närmaste omöjligt. Man måste
åtminstone lägga något mått av tid och energi på lärandet, och åtminstone måste
någon möda gå åt till att lagra ny information.

Hur enkelt det är att lära sig något beror på vad förutsättningarna är. Vissa
verkar ha fallenhet för specifika områden som matematik, språk eller konst; och
ibland går det lättare att lära sig något än annars, kanske beroende på vilken
lärare man har. Men riktigt hur dessa förutsättningar kan ges på bästa sätt är,
tyvärr, en fråga utan ett bra svar. Det finns många teorier kring lärande, och
man kan dessutom studera lärandesituationer och dra slutsatser kring dessa.

Som underlag för denna diskussion har ett undervisningspass hållits, som en del
av mentorskapet i Intize. Planeringsfasen, utförandet samt resultatet kommer
analyseras och jämföras med några av de mest kända teorierna om utlärning.

I mentorskapsgruppen jobbar adepterna med olika delar av gymnasiematematiken,
och befinner sig dessutom på olika nivåer av förståelse för deras respektive
delområde. För att undvika scenarion där passet behandlar delar som är över
någon adepts matematiska färdigheter, eller långt under, så skall undervisningen
beröra ett område ingen av dem kommit i kontakt med (men som inte heller är för
avancerat för dem att kunna ta till sig).

För detta ändamål valde jag att ha en genomgång av binära tal, eller (svårligen
urskiljaktigt) \emph{positionstalsystem}. Detta är ett område av matematik som
många hört talas om, men som få på gymnasienivå har jobbat med speciellt
ingående. Själva matematiken är egentligen inte så svår -- tröskeln ligger mest
i en ovana vid att jobba med andra talbaser än tio. Därför ser jag detta som ett
område som passar väl för gruppen. Vidare är det min uppfattning att en djupare
kunskap för hur olika representationer av tal fungerar starkt underlättar, även
när man söker förståelse inom andra delar av matematik.


\section{Teoretiska utgångspunkter}
\label{sec:theory}

Denna rapport kommer att utgå ifrån ett antal teorier, så som de presenteras i
boken \emph{Perspektiv på lärande}~\citep{ppl}.

Konstruktivism är en av de teorier som intresserar mig mest, och därför ämnar
jag lägga mest fokus på att testa och utvärdera denna teori. Det som gör den
extra intressant i mina ögon är att den väl beskriver hur jag upplever att mina
egna minnen lagras, representeras och ser ut. Specifikt vill jag prova att lära
ut genom att först presentera kända delar av adepternas ''mentala karta'', så
att de får en referenspunkt att utgå ifrån när man sedan tränger in på okänt
territorium.

Liksom Piaget, så menar jag att små delta är fördelaktigt för inlärning. En av
anledningarna att versionshanteringsprogrammet Git är så överlägset alla andra
versionshanteringsprogram är att Git endast lagrar mellanskillnaden, det så
kallade deltat, mellan två versioner av en fil. All information som är identisk
mellan två versioner av en fil tillgängliggörs genom en pekare. Utan att behöva
ge informationsteoretikerna rätt så är det naturligtvis så att information är
information, oavsett om i en hjärna eller en dator. Därför hoppas jag kunna se
att det analogt följer att små mängder ny information åt gången är lätt att ta
till sig.

Denna metod av att bryta ned saker i sina beståndsdelar och diskutera
konsekvenserna av varje delsteg gör det väldigt naturligt att med ledande frågor
föra adepterna på rätt väg. I detta kommer jag följa den gestaltteoretiska
ansatsen, och ge alla förutsättningar som krävs för att lösa problemet. Däremot
kommer jag inte göra någon ansats att djupare analysera detta kriterium.

Slutligen vill jag också analysera den utlärningsmetod \citet{Muller2008} lägger
fram. Kort sagt kretsar denna metod kring att först diskutera elevens eventuella
felaktiga uppfattningar, för att sedan visa varför de är falska och slutligen
presentera den riktiga förklaringen. Dr. Muller visar resultat som starkt
påvisar en positiv förändring i utlärning av fysik, enligt denna metod.


\section{Metod}
\label{sec:method}

Innan föreläsningen börjar skall jag försöka berätta lite om binära tal,
exempelvis att de är grunden för hur datorer fungerar. Detta efter tipset från
\citet{jana}, om att börja med att berätta vad det är $x$ gör, innan man går in
på detaljer.

Jag börjar med att skriva ett tal (exempelvis $23$) på tavlan, och ställa den
närmast retoriska frågan \textit{"Vad står det här?"}. När de ger rätt svar, så
kommer följdfrågan \textit{"Varför?"}. På så sätt hoppas jag leda in
diskussionen på positionstalsystem, och hur de fungerar. Specifikt planerar jag
att börja med decimaltal.

I princip alla i vårt samhälle är mycket vana vid hur det decimala talsystemet
är uppbyggt, så för att ge en känd referenspunkt väljer jag att först resonera
lite kring hur ''vanliga'' tal fungerar. Enligt konstruktivistiska teorier är
familjära områden på den ''mentala kartan'' en bra utgångspunkt för att lära ny
kunskap. Jag uppskattar personligen när föreläsare själva tar perspektivet av
åhöraren, då jag många gånger upplevt att en sådan retorik som novis är lätt att
ta till sig. Detta går i linje med vad \citet{kernell} menade under sin
föreläsning på temat \emph{Möten mellan människor}.

Två av de centrala punkterna i min genomgång kommer vara \emph{magnitud} och
\emph{siffror}. Att kunna beräkna talvärdet utifrån siffervärdet tillsammans med
talpositionen är självklart grunden för att kunna förstå tal i någon bas. Vad
som däremot kom som lite av en överraskning under förberedelserna var hur
bortglömd siffran är. \emph{Varför behöver man en ny siffra på positioner
    $>N-1$?} fick jag fråga mig själv, och jag valde att förklara det genom visa
vad som händer när man försöker klämma in två siffror på samma talposition;
visst \emph{funkar} det, men det är svårt att läsa och man kan representera
talvärdet i termer av bara en siffra per position istället.

Därefter skall det binära talsystemet presenteras. Analogt med hur
decimaltalsystemet presenterades, skall basen två användas. Jag skall resonera
på ungefär samma sätt som tidigare, kring vad en siffra är, hur man utläser ett
tal osv.

I detta stadium blir återkoppling viktigt, så att jag förstår att de fortfarande
hänger med. Om så inte är fallet hoppas jag kunna förklara de delar som är
oklara.

För att sätta Mullers teori i praktik skall jag ''av misstag'' sätta in siffran
$2$ i ett binärtal, för att visa vart det leder (detta är visserligen inte
strikt otänkbart, men högst okonventionellt och tvetydigt).

Efter att visa dem lite olika tal i termer av 1 och 0, samt övergången mellan
bas 10 till bas 2 och vice versa, ämnar jag avsluta med en praktisk övning --
jag vill försöka lära dem hur man räknar binärt på fingrarna. Det är en
färdighet jag själv övat in, och även om det mer är en kul gimmick än något
användbart så tror jag det är pedagogiskt att låta folk jobba med händerna. Det
är inte så ofta man kan räkna på fingrarna för att lära sig ny matematik när man
kommit till lite mer avancerad nivå, så man får ta tillfället i akt!


\section{Resultat}
\label{sec:result}

Mentorsträffen genomfördes i princip enligt planerad struktur. Alla delar som
behandlade det decimala talsystemet verkade tas emot väl av adepterna. Segmenten
som berörde lite mer generella koncept inom positionstalsystem trängtade knådas
lite extra, men även detta verkade accepteras förhållandevis väl. Däremot visade
det sig vara mycket mer svårsmält när vi väl kom fram till binärtalen.

Jag förväntade mig att binära tal skulle vara ovant för dem, men inte svårt.
För mig, som har god förståelse i ämnet, känns det som att multiplikationen $1
\cdot 2^n$ inte borde vara speciellt svår, men där visade det sig att jag hade
fel. Adepterna verkade förstå konceptet om $2^n$ och positionsvärdet ($1, 2, 4,
8, \dots$) var för sig, men de såg inte kopplingen väl nog för att se det
naturliga sambandet i systemet.

Att få dem att acceptera varför den högsta siffran i en talbas är $N-1$ var
svårare än jag trodde, och jag vet inte om jag lyckades övertyga dem.

När jag slutligen visade dem hur man kan räkna binärt på fingrarna trodde jag
för ett ögonblick att jag lyckats hitta en passande utlärningsmetod. Ända upp
till 7 klarade vi av att räkna tillsammans, och de gav intryck av att faktiskt
ha förstått. Tyvärr förstod jag så småningom att de inte så mycket förstod
systemet, som att de memorerade hur permutationerna av fingerkombinationer
mappade till talvärden. Imponerande vilken mental kapacitet man kan lägga på att
komma runt ett problem på det svåra sättet!

Min analys i efterhand är att de inte kunde ta till sig mycket av det jag
visade. Mest på grund av bristande motivation; att de inte ''behöver'' kunskapen
för att klara en kurs i skolan. Att de verkade acceptera mycket av de ganska
avancerade koncept jag rörde vid med decimaltal kan ha varit en illusion. Jag
menar inte att de ljög för mig när de sa att de förstod, utan att de trodde de
förstod något de inte faktiskt förstod. De är så vana vid decimaltal att de vet
hur de fungerar utan att nödvändigtvis förstå alla delar; därför kan de ha haft
illusionen av att förstå konceptet trots en egentligen bristande förståelse.

Det de däremot tog till sig, verkar ha lärts in på två olika sätt: teorin bakom
hur decimaltalsystemet fungerar tycktes bygga ganska naturligt från deras
tidigare kunskaper, och bit för bit utöka den; det binära systemet däremot
verkar ha lärts in om statisk kunskap, ungefär som \textit{''Sydney är den
    äldsta och största staden i Australien''} -- \textit{''1 betyder 1, 10
    betyder 2, 11 betyder 3, \dots''}.


\section{Diskussion}
\label{sec:discussion}

Det verkar som att adepterna inte tillgodogjort sig någon djup förståelse för
området som presenterades, vilket är beklagansvärt. Men det är inte speciellt
förvånande, med tanke på den osympatiska situationen att lära ut till någon som
egentligen inte känner sig intresserad av ämnet. I mina ögon blev hela
undervisningspasset en aning krystat, som följd av passets syfte att ligga till
grund för denna analys. Men -- bortkastat var det inte. Det finns flera detaljer
att se och slutsatser som kan dras.

Konstruktivismens stora fördel -- att ge en munsbit i taget -- kan tydligt
identifieras. Tyvärr är även dess nackdel uppenbar: adepterna hade svårt att se
kopplingen mellan varje del, och fick ingen god bild av hur helheten fungerade.
Adepterna uppvisade förståelse för att siffror på olika positioner får olika
värden, de kunde multiplicera siffervärdet med positionsvärdet, och de kunde
också summera talvärdet. Var för sig innebar detta inga utmaningar, men som en
helhet uppstod svårigheter för adepterna att förstå teorin och omsätta den i
praktik.

Det är svårt att tänkta sig något annat än att lära sig i små delsteg är mindre
krävande än att lära sig helheter direkt. Men att något är enklare innebär inte
med nödvändighet att det är bättre. När man lär sig små steg i taget kan det
vara svårt att få en god överblick av problemet, och ibland kan man glömma vad
målet är. Det blir lite som att få små bitar av en karta, tillsammans med en
instruktion som beskriver vilken ordning man ska sätta ihop dem. Det är för
visso allt som krävs för att lösa problemet, men föga pedagogiskt.

För att lösa den problematiken bör man då och då ta ett steg tillbaka, för att
försöka se framåt på målet. Om man påminner om var man var och vart man ska, så
blir det lättare att se hur pusselbiten man just fick passar in.

Jag misstänker att utgångspunkten i den mentala kartan av decimaltalsystemet
faktiskt kan ha lett dem på fel spår. Det är fördelaktigt att på förhand ha en
god bild av ett område, för att lära nytt. Däremot gäller det att inte ha en
felaktig karta. Om man har det, kan istället beskrivningar och koncept bli
märkliga och förvridna, när de anpassas för att passa in i den felbild man
sitter inne med. Adepterna är förmodligen så vana vid att använda det decimala
talsystemet, att när jag förklarade teorin bakom det så fick de en illusion av
att förstå helheten. De såg början, och de såg målet; tyvärr leddes de att
felaktigt extrapolera vägen däremellan.

Metoden att först visa på vad presumtiva missuppfattningar kan leda till, för
att sedan leda på rätt väg, fick tyvärr inte nog med utrymme för att kunna dra
någon slutsats av.

Jag kan i efterhand se att ett av de bästa besluten jag tog var att försöka få
återkoppling från adepterna under lektionens gång. Ju bättre uppfattning om
elevernas förståelse man har, desto lättare blir det att fånga upp detaljer man
kanske har förbisett i planeringen, eller bristande förkunskaper som behöver
hanteras.


\section{Slutsats}
\label{sec:conclusion}

Jag kan inte lära dig någonting. Inte heller kan du lära mig någonting. Lärande
är en enkelriktad process, och den enda som kan lära någon något är eleven
själv. En lärares uppdrag kan, ur det perspektivet, tyckas synnerligen
osympatiskt. Lyckligtvis betyder inte detta att lärarens uppgift är meningslös.
Lärarens roll är att ge eleven så bra förutsättningar som möjligt för lärande,
även om det slutligen är upp till eleven att tillgodogöra sig ny kunskap.

Att hjälpa någon till ökad kunskap inom ett område hänger på att personen
faktiskt \emph{vill} lära sig. Detta är något jag misstänker att varje lärare i
Sverige är smärtsamt medveten om. Utan tvekan är detta ett område som det
svenska utbildningssystemet kan utvecklas mycket inom -- och kanske främst när
det kommer till matematik. Gymnasiematematiken är inte för svår för någon
normalbegåvad människa att lära sig, men den kräver ändå ett visst intresse för
att man som elev alls ska kunna motivera sig själv till att lägga ned den tid
och energi som krävs för att lära sig.

Ordet \emph{privatlärare} så gott som skimrar av att ge goda förutsättningar --
att som elev ha en lärare dedikerad helt till en själv är av stort värde. En av
anledningarna menar jag är hur mycket lättare det är att på ett användbart sätt
få återkoppling, för att anpassa undervisningen. I en stor grupp, som i en
gymnasieklass, så är det närmast omöjligt att ta hand om alla elevers bristande
förkunskaper och feluppfattningar på det sätt man kan göra i en liten grupp
(eller med en enskild person). Och detta har vi ju tydligt sett är av stor vikt.


\bibliography{../ref}

\end{document}
