\documentclass[a4paper]{article}

%% Always use these:
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[a4paper,margin=1in]{geometry}

\usepackage[swedish]{babel}
\usepackage[iso,swedish]{isodate}
\usepackage{icomma}

%% Usually preferred packages:
\usepackage[hidelinks]{hyperref} % Usage: \url{}

\usepackage{natbib}
\bibliographystyle{abbrvnat}
\setcitestyle{authoryear,open={(},close={)}}
\let\cite\citet

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[RE,RO]{\textit{ID: 202}}
\fancyhead[LE,LO]{\leftmark}

\title{Inlämning 1\\
    \small{MVE370 -- Mattematik och Samhälle}}
%\date{}
\author{ID: 202}

\begin{document}

\maketitle

% \tableofcontents
% \newpage

% \begin{abstract}

% Genom alla tider har människan samarbetat för att utvecklas, både individuellt
% och som samhälle. Genom generationer förs kunskap vidare, på ett eller annat
% sätt. Olika situationer kräver olika sätt att förmedla kunskap.

% Inom yrkessektorn är lärlingskapet en viktig del av utbildningen, medan det inom
% den akademiska världen är lärarrollen som dominerar. Men gemensamt för dessa
% utlärningsformer är mentorskapet; att mindre erfarna får möjligheten att lära
% sig av mer erfarna. Inom mentorskapet finns alltid med något inslag av praktik
% där adepten, genom någon grad av ledning från mentorn, utför en uppgift eller
% löser ett problem, i syfte att vinna nya färdigheter.

% \end{abstract}

\section{Mentorskapets förutsättningar}

Mentorskap är ett utlärandesätt, i gränslandet mellan en lärares undervisning
och en mästares lärlingspraktik, där en \emph{adept} får hjälp av en
\emph{mentor} för att utveckla sina färdigheter inom något område.

Essensen inom mentorskapet är samarbetet mellan mentor och adept, där de båda
strävar efter samma mål; adeptens utveckling. Utan ett välfungerande samarbete
kan inte mentorn hjälpa adepten att utvecklas. Traditionell undervisning är inte
på samma sätt sårbar för elevens ovilja som mentorskapet är. En lärare kan
undervisa, oavsett om eleverna deltar aktivt eller om de sover. Resultatet blir
självfallet inte det samma, men lärarens roll är spelad. En mentor kan däremot
aldrig leda en adept framåt utan dennes samarbete.

Fördelen med mentorskap är just det samma som nackdelen -- det kräver mer tid
och engagemang, både från mentor och adept. Att mentorsarbetet av sin natur
kräver mer arbete för att komma någonstans medför bättre förutsättningar för det
personliga lärandet. Detta både tack vare det personliga mötet (möjligheten för
adepten att ställa specifika frågor; få hjälp med individens specifika
svårigheter) och det faktum att mer tid läggs ned på lärandet. Ju mer man
arbetar med något desto bättre blir man på det.


\subsection{Att få mentorskap}
\label{sec:adept}

\textit{Mentorskap är inte något man tar, det är något man får.}
%
Oavsett hur duktig man än är så kommer man någon gång hamna i situationen att
man inte kan klara en uppgift på egen hand. När man står inför en uppgift som är
så svår att man inte kan klara den på egen hand så har man två alternativ, om
uppgiften ska bli gjord; be någon annan, som är mer kapabel, att lösa uppgiften,
eller att lära sig hur man löser uppgiften. Det förstnämnda har så klart
fördelen att uppgiften kan bli utförd relativt snabbt, och man slipper lägga ned
egen tid och energi på det. Detta kan vara ett bra val, om uppgiften i fråga är
av en engångskaraktär. Att däremot om och om igen vara beroende av andras
färdigheter och vilja att hjälpa kanske inte är helt idealiskt. Då står det
andra alternativet kvar: lär dig!

Men att lära sig något nytt är lättare sagt än gjort många gånger. I vissa fall
kan man ta del av andras undervisning; man kan läsa litteratur, lyssna på
föredrag eller på annat sätt ta del av andras kunskap. Men utan tillräckliga
erfarenheter kanske inte denna typ av kunskap räcker. Då krävs det att man ber
om hjälp.

Hjälp finns i flera former. \citet{schein09} delar upp hjälp i tre kategorier:
informell, semiformell och formell hjälp. Dessa uppdelningar är kort sagt ett
mått på hur hög tröskeln för att få hjälp är.

Informell hjälp är den typ av hjälp som få ber om, men många förväntar sig.
Kanske kan man kalla denna typ av hjälp \emph{vedertagen hjälp}. Detta kan vara
mer eller mindre alldagliga saker (som att hålla upp en dörr åt någon annan,
eller hjälpa en medmänniska i akut nöd), där tjänsten visserligen förväntas men
inte kommer efterfrågas. Bara för att denna typ av tjänst inte efterfrågas
betyder det inte att den är oviktig -- i ett samhälle finns sociala normer som
förväntas följas -- det betyder bara att man inte gärna kan frambringa den
genom externa medel så som pengar. % ordbajs

Semiformell hjälp beskriver Schein som ``techsupport''; man har förvisso ett
problem, men man söker en lösning på problemet snarare än ett sätt att lösa det.

Formell hjälp, slutligen, är tjänster som explicit efterfrågas för att lösa ett
problem som man inte kan lösa på egen hand. Den största skillnaden mellan denna
och tidigare kategorier är fokuset på personlig utveckling. Det kan handla om
att lära en ny färdighet eller utvecklas fysiskt eller mentalt (ofta inom vård);
där vi själva har en svårighet som vi önskar övervinna på ett eller annat sätt,
men inte själva är kapabla.

Denna sista kategori, formell hjälp, är i mångt och mycket den kategori av hjälp
som mentorskap landar i; att be om någon annans hjälp för att själv utvecklas på
personligen.

Mentorskapets ömsesidiga natur gör det omöjligt att ta mentorskap. Istället
måste man be om det. Ibland kan pengar eller andra ``mutor'' vara del av att
övertyga mentorn, men slutligen måste mentorn ändå vara villig att lägga ned den
tid och energi det kostar att handleda adepten. 

Att be om någons hjälp, och därmed indirekt visa sig otillräcklig i någon grad,
är det första steget i mentorskapet. Det är en utsträckt hand, och utan ett
gensvar kommer inte mentorskapet längre än så. Att som adept ödmjuka sig genom
att gå till en mentor för att få hjälp är en viktig del av gruppdynamiken mellan
de två, vilket diskuteras mer under avsnitt~\ref{sec:gruppdynamik}.


\subsection{Att vara mentor}
\label{sec:mentor}

\textit{Mentorskap är inte något man gör, det är något man ger.}
%
Som mentor behöver man för det första tänka på att vara mild i mottagandet av
adepten. Det är mycket möjligt att adepten inte alls är nämnvärt sårbar --
kanske är det rent av därför denne alls vågar söka mentorskap. Men i de fall då
adepten känner sig svag i egenskap av någon som behöver be om hjälp så hänger
mycket av mentorns framtoning på att bemöta adepten på rätt sätt.

Liksom för den som ber om hjälp så hänger mentorns prestation på den ömsesidiga
relationen mellan de två parterna. Utan en villig motpart har mentorn inget att
komma med. Men mentorn kan självklart göra olika mycket för att se till att
samarbetet fungerar väl.

Vidare är det viktigt att mentorn klarar av att på ett korrekt sätt avgöra
adeptens behov och förmågor. Möjligen kan adepten själv beskriva var på
inlärningskurvan denne befinner sig, men mentorns större erfarenhet och kunskap
inom ämnet kan ha mycket att tillföra för att avgöra detta.


\subsection{Gruppdynamiska förutsättningar}
\label{sec:gruppdynamik}

Förutsättningen, som jag påtalat tidigare, är ett väl fungerande samarbete
mellan mentor och adept. I ett mentorskapsarbete finns olika roller.
Naturligtvis har mentorn någon form av ledarroll, men riktigt hur detta yttrar
sig beror på mentorn och gruppen.

I vissa fall kan mentorn inta rollen av en stark ledare: mentorn vet vart man är
på väg, och mentorn bestämmer hur adepten ska arbeta för att uppnå de mål som
finns. Enligt Thomas--Kilmann-modellen för konflikthantering, som Anneline
Sander tog upp i sin föreläsning, skulle denna typ av mentor kunna klassas som
en ``drivare''. Nackdelen är att då adepterna inte får mycket att säga till om,
och om de som en konsekvens av detta känner att deras röst inte blir hörd
riskerar gruppen att hamna i en situation där adepterna slutar lyssna på
mentorn eftersom de ändå bara känner sig ignorerade om de gör det. Fördelen är,
förutsatt att mentorn kan ämnet i fråga väl och har en god didaktisk stil, att
gruppen blir lätt att styra. En stark ledare kan på förhand planera hur vägen
mot adeptens mål ska se ut, och om rollen hanteras väl kan mentorn få en
auktoritetsroll gentemot adepten, vilket leder till bättre gehör från dennes
sida.

Motsatsen till detta är en platt organisation, där ledaren (mentorn) sätter sig
på samma nivå som adepterna. Ordet ``platt'' organisation leder tanken till en
filt (som ju är ganska platt). Man kan föreställa sig att ledaren är mittpunkten
på filten, och att man där nyper tag för att låta ledaren välja en riktning att
röra sig i. När man flyttar på ledar-punkten kommer resten av filten att dras
med, men alla punkter på ytan kommer inte röra sig likadant. Vissa rör sig
väldigt likt ledaren, medan vissa sackar efter, eller kanske rör sig i en annan
riktning. Men summan av rörelsen kommer ändå vara samma som ledarens, och det är
en rörelse som ger alla tid att anpassa sig. Nackdelen är att det kanske tar ett
litet tag innan alla punkter rör sig i rätt riktning.

En annan nackdel, om vi går ifrån liknelsen för ett slag, är att platt ledarskap
riskerar att sätta mentorn i en ``kompisroll'' i förhållande till adepterna. En
kompis kan visserligen vara kul att snacka med, och förmodligen lyssnar man på
sina kompisar mer än på främlingar, men om det går för långt kan det leda till
att adepterna tappar respekt för mentorns auktoritet och slutar lyssna.
Speciellt i situationer där adepten själv inte är drivande i sin utveckling
riskerar detta att bli ett problem.

I verkligheten är olika typer av ledarskap en glidande skala, och det är sällan
eftersträvansvärt med extremfallen. I liknelsen med filten kan man tänka sig att
man lyfter ledar-punkten olika högt, som en dimension av hur direktstyrt
ledarskap en grupp har. I en grupp med en stark ledare är punkten lyft högt, och
resten av filten hänger kring den punkten, och flyttas mer eller mindre
parallellt med ledaren. Det andra extremfallet diskuterade vi tidigare. Ofta
behöver ledaren i gruppens tidiga fas känna in vilket typ av ledarskap som
krävs, och anpassa sig efter den miljö som råder (detta i samklang med
FIRO-modellens första fas). Olika stilar är bra för olika grupper.

Mentorskapet i Intize är av en speciell karaktär -- det är ett ideellt drivet
mentorskap med ett mål som hela gruppen kan sträva mot men aldrig nå: varje
adepts personliga utveckling inom matematik. 

Den första karaktäristiken, att mentorskapet är ideellt, medför en personligare
relation mellan mentor och adept än ett betalt mentorskap ofta gör. Eftersom
båda parter förlitar sig på de andra i gruppen för det gemensamma målets
eftersträvan drivs gruppen till stor del av grupptryck och ansvarskänsla. Att i
denna situation driva en stark ledarstil riskerar lätt att distansera mentor
från adepter onödigt mycket, och därmed försvaga gruppkänslan som behövs för
gruppens drivkraft. 

Intize-mentorskapets inriktning på personlig utveckling kan göra det svårt för
gruppen att få en stark känsla av sammanhållning, såvida inte deltagarna sedan
känner varandra. Detta är en av trösklarna som måste övervinnas i varje
mentorsgrupp för en lyckad utveckling.


\section{Nulägesanalys}

Min mentorsgrupp befinner sig ännu i FIRO-modellens initiala fas. Det var en
ganska lång period i början av terminen där adepter var svåra att kontakta, och
vissa förlorade sin plats som konsekvens av detta, vilket ledde till att gruppen
nybildades flera gånger.

Det är fortfarande svårt att få hela gruppen att samlas, vilket tyder på att
gruppkänslan inte är en stark faktor än. Min erfarenhet av andra grupper jag
varit ledare för är att gruppens sammanhållning naturligt blir starkare med
tiden, förutsatt att inga interna komplikationer uppstår (så som aggression
mellan gruppmedlemmar). Detta hänger dock på att gruppen träffas och umgås
tillräckligt frekvent för att bilda band, samt en vana att komma på träffarna.
Utan att göra träffarna till en vana kommer det inte kännas som att man bryter
ett mönster när man uteblir. Utan en sådan känsla minskar motivationen att komma
på träffarna regelbundet, och om man får alla att komma varje gång har man
lyckats instifta en form av makt över adepterna genom att få dem att lyda påbud.

Jag skulle klassificera Intize-mentorskaps hjälpstil från två perspektiv:
externt, att komma till en läxhjälp för att få ledning av en mentor; och
internt, hjälpstilen i just min grupp. Externt är mentorskapet helt klart en
formell hjälp -- adepterna ber om  hjälp för att utvecklas på ett personligt
plan. Internt däremot, i min grupp, råder en mer informell hjälpstil där varje
adept ber mig om hjälp när de fastnar på en uppgift eller inte förstår ett visst
koncept. Jag har valt att ha en ganska platt hierarki, just för att sänka
tröskeln för dem att fråga frågor. I och med att alla i min grupp går i olika
klasser och jobbar med olika problem blir det inte så naturligt att hålla
föreläsningar (eftersom jag omöjligen kan inkludera allas matematiska
studieområden). Då blir en stark ledarroll mest ett hinder, eftersom distansen
mellan mig och mina adepter blir större och därmed även tröskeln för dem att be
mig om hjälp.

\section{Gruppens vidareutveckling}

Min förhoppning är att förbättra disciplinen att komma på träffar nästa år,
genom att trycka på mentorskapets ideella form, och på så sätt ålägga ett visst
socialt tryck om samarbete från deras sida. Jag räknar med att det inte kommer
bli några större problem att få dem att samarbeta bättre på det planet -- även
de är ju här på frivillig basis, det skulle vara märkligt att då inte samarbeta.

\section{Min utveckling}

Jag har varit ledare i många sammanhang, främst inom ungdomsarbete samt arbete
inom diverse styrelser, och känner att jag har ganska mycket erfarenhet sedan
tidigare av ledarskap. Det är alltid utvecklande att få jobba med en ny grupp,
och även om jag inte fått speciellt mycket ny erfarenhet av just ledarskapet så
är rollen som lärare ny för mig. Vi har i kursen inte berört didaktik än, men
min förhoppning är att den delen av kursen kommer innefatta fler delar jag
inte är bekant med sedan tidigare.

Att vara ledare för en grupp är unikt för varje sammanhang, och det är svårt att
säga en allmängiltig regel för hur man ska bete sig i en grupps uppstartsfas.
Men ju fler gånger man är ledare, och ju mer olika de grupper man är ledare för
är, desto större repertoar av erfarenheter bygger man. Med mer erfarenhet blir
det lättare att snabbt göra en korrekt analys av gruppen, för att agera
därefter.

\bibliography{../ref}

\end{document}

Bedömning
Uppgiften betygssätts efter en tvågradig skala:  Godkänd och Underkänd.
För betyget Godkänd krävs:

* Att du har besvarat de givna frågeställningarna
* Att det framgår att du studerat kurslitteraturen och att din analys bearbetar det du fått
med dig därifrån
* Att argumentationen är logisk och texten är väl organiserad och tydlig
* Att du håller dig till 2­4 sidor med rimligt typsnitt, radavstånd och marginaler
* Att du lämnar in din essä och eventuell retur i tid

För att bli godkänd måste du följa Harvards referenssystem
